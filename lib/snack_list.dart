import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/cart_model.dart';
import 'package:shop/cart_provider.dart';
import 'package:shop/cart_screen.dart';
import 'package:shop/db_helper.dart';

class SnackListScreen extends StatefulWidget {
  const SnackListScreen({Key? key}) : super(key: key);

  @override
  _SnackListScreenState createState() => _SnackListScreenState();
}

class _SnackListScreenState extends State<SnackListScreen> {


  List<String> productName = ['Potato chips', 'Filled Cookies', 'Chocolate Bar', 'Candy', 'Frozen Chocolate Cake', 'Chewing gum', 'Wafer', 'Cookie', 'Cashews', 'Almonds',] ;
  List<String> productUnit = ['Slice', 'Slice', 'Pack', 'Slice', 'Slice', 'Slice', 'Slice', 'Slice', 'Slice', 'Slice',] ;
  List<int> productPrice = [20, 20, 25, 45, 110, 12, 65, 72, 15, 25 ] ;
  List<String> productImage = [
    'https://backend.tops.co.th/media/catalog/product/8/8/8850718801213_10-01-2022.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/4/8/4893049150012_11-05-2022.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/9/5/9556001071750_15-11-2021.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/0/0/0000096083864_1.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850231000582.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8852008610130_1.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/0/8000380157549_10-02-2022.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850332251111_12_03-2021.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850291210464_1.jpg?impolicy=resize&height=300&width=300',
    'https://backend.tops.co.th/media/catalog/product/8/8/8850096870092_e13-07-2020.jpg?impolicy=resize&height=300&width=300',
  ] ;

  DBHelper? dbHelper = DBHelper();

  @override
  Widget build(BuildContext context) {
    final cart  = Provider.of<CartProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Snack Product List'),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: (){
              Navigator.push(context,MaterialPageRoute(builder: (context) => CartScreen()));
            },
            child: Center(
              child: Badge(
                showBadge: true,
                badgeContent: Consumer<CartProvider>(
                  builder: (context, value , child){
                    return Text(value.getCounter().toString(),style: TextStyle(color: Colors.white));
                  },
                ),
                animationType: BadgeAnimationType.fade,
                animationDuration: Duration(milliseconds: 300),
                child: Icon(Icons.shopping_bag_outlined),
              ),
            ),
          ),

          SizedBox(width: 20.0)
        ],
      ),

      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.green),
              accountName: Text(
                "MySnack Shop",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                ),
              ),
              accountEmail: Text(
                "snackshop2022@gmail.com",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/images/store-icon-svg-png.png'),
                radius: 60,
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            AboutListTile( // <-- SEE HERE
              icon: Icon(
                Icons.account_circle,
              ),
              child: Text('About'),
              applicationIcon: Icon(
                Icons.account_circle,
              ),
              applicationName: 'About',
              aboutBoxChildren: [
                Image.asset("assets/images/IMG_20220718_160420.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("คนที่ 1",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),),
                Text("รหัส : 6350110005",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),),
                Text("ชื่อ : ณัฐนนท์ ส่งศรีจันทร์",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),),
                SizedBox(height: 20,),
                Image.asset("assets/images/293039182_2501543417359842_2713414559648052017_n.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("คนที่ 2",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),),
                Text("รหัส : 6350110014",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),),
                Text("ชื่อ : พงศ์อรรถ แก่นอิน",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),),
              ],
            ),
          ],
        ),
      ),

      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: productName.length,
                itemBuilder: (context, index){
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Image(
                                height: 100,
                                width: 100,
                                image: NetworkImage(productImage[index].toString()),
                              ),
                              SizedBox(width: 10,),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(productName[index].toString() ,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(height: 5,),
                                    Text(productUnit[index].toString() +" "+r"$"+ productPrice[index].toString() ,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(height: 5,),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: InkWell(
                                        onTap: (){
                                          print(index);
                                          print(index);
                                          print(productName[index].toString());
                                          print( productPrice[index].toString());
                                          print( productPrice[index]);
                                          print('1');
                                          print(productUnit[index].toString());
                                          print(productImage[index].toString());

                                          dbHelper!.insert(
                                              Cart(
                                                  id: index,
                                                  productId: index.toString(),
                                                  productName: productName[index].toString(),
                                                  initialPrice: productPrice[index],
                                                  productPrice: productPrice[index],
                                                  quantity: 1,
                                                  unitTag: productUnit[index].toString(),
                                                  image: productImage[index].toString())
                                          ).then((value){

                                            cart.addTotalPrice(double.parse(productPrice[index].toString()));
                                            cart.addCounter();

                                            final snackBar = SnackBar(backgroundColor: Colors.green,content: Text('Product is added to cart'), duration: Duration(seconds: 1),);

                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);

                                          }).onError((error, stackTrace){
                                            print("error"+error.toString());
                                            final snackBar = SnackBar(backgroundColor: Colors.red ,content: Text('Product is already added in cart'), duration: Duration(seconds: 1));

                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                          });
                                        },
                                        child:  Container(
                                          height: 35,
                                          width: 100,
                                          decoration: BoxDecoration(
                                              color: Colors.green,
                                              borderRadius: BorderRadius.circular(12)
                                          ),
                                          child: const Center(
                                            child:  Text('Add to cart' , style: TextStyle(color: Colors.white),),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )

                            ],
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),

        ],
      ),
    );
  }
}